""" A simple module to colorize shell output"""

class Bcolors:
    HEADER = '\033[95m'  # Violet
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'  # Jaune
    FAIL = '\033[91m'  # Rouge
    INFO = '\033[33m'  # Orange
    ENDC = '\033[1;m'


class Printer:
    verbose = 1
    syslog = False
    debug = False
    color = True

    def __init__(self):
        self.__class__.verbose = 1
        self.__class__.color = True

    @classmethod
    def colorize(self, color, value):
        return color + value + Bcolors.ENDC

    @staticmethod
    def sanitize(message):
        # Escape bracket braces because of format
        message = message.replace('{', '{{')
        message = message.replace('}', '}}')
        return message

    @staticmethod
    def warning(level, msg, *highligh):
        if Printer.color:
            msg = "[" + Printer.colorize(Bcolors.WARNING, "-") + "] " + msg
        else:
            msg = "[-]" + msg
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.WARNING + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        if Printer.verbose >= level:
            print(msg.format(*collored))

    @staticmethod
    def okblue(level, msg, *highligh):
        if Printer.color:
            msg = "["+Printer.colorize(Bcolors.OKBLUE, "~")+"] " + msg
        else:
            msg = "[~] " + msg
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.OKBLUE + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        if Printer.verbose >= level:
            print(msg.format(*collored))

    @staticmethod
    def okgreen(level, msg, *highligh):
        if Printer.color:
            msg = "[" + Printer.colorize(Bcolors.OKGREEN, '+') + "] " + msg
        else:
            msg = "[+] " + msg
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.OKGREEN + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        if Printer.verbose >= level:
            print(msg.format(*collored))

    @staticmethod
    def info(level, msg, *highligh):
        if Printer.color:
            msg = "[" + Printer.colorize(Bcolors.INFO, "#") + "] " + msg
        else:
            msg = "[#] " + msg
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.INFO + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        if Printer.verbose >= level:
            print(msg.format(*collored))

    @staticmethod
    def debuger(msg, *highligh):
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.HEADER + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        print("[" + Bcolors.HEADER + "DEBUG" + Bcolors.ENDC + "] " +
              msg.format(*collored))

    @staticmethod
    def fail(msg, *highligh):
        msg = "[" + Printer.colorize(Bcolors.FAIL, "x") + "] " + msg
        collored = list()
        for i in highligh:
            if Printer.color:
                collored.append(Bcolors.FAIL + str(i) + Bcolors.ENDC)
            else:
                collored.append(str(i))
        print(msg.format(*collored))
